# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
{
    'name' : 'Customer_sales',
    'version' : '1.0',
    'summary': 'Customer_sales',
    'sequence': 17,
    'description': """

    """,
    'category': 'Customer sales  Management',
    'website': 'https://www.emiprotechnologies.com',
    'images' : [],
    'depends' : ['sale_management','sale_stock'],
    'data': [
        'security/ir.model.access.csv',
        'views/customer_sale_view_ept.xml',
        ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
