from odoo import models,fields,api

class CustomerSale(models.Model):
    
    _name="customer.sale.rel.ept"
    _inherit = ['mail.thread']
    _rec_name = 'customer'
    sale_order_id=fields.Many2one('sale.order',string="Sale Order")
    date=fields.Datetime(string="Date",default=fields.Datetime.now)
    customer=fields.Char(string="Customer")
    warehouse=fields.Char(string="warehouse")
    invoice_paid_amount=fields.Float(string="invoice paid amount",compute='_amount_all')
    invoice_remain_amount=fields.Float(string='invoice remain amount',compute='_amount_all')
    products=fields.Many2many('product.product',string='Products')
    


    def _amount_all(self):
        """
        Compute the total invoice amounts  of the SO.
        """
        for rel in self:
            rel.invoice_paid_amount = rel.sale_order_id.invoice_ids.amount_total_signed-rel.sale_order_id.invoice_ids.residual_signed
            rel.invoice_remain_amount=rel.sale_order_id.invoice_ids.residual_signed
        
#         amount_paid_total = amount_remain_total = 0.0
#         for order in self:
#             amount_paid_total += order.invoice_paid_amount
#             amount_remain_total += order.invoice_remain_amount
            
             
#     amount_paid_total = fields.Float(string='Paid_Total', readonly=True,compute='_amount_all' )
#     amount_remain_total = fields.Float(string='Ramain_Total', readonly=True, compute='_amount_all')


    
    
    @api.onchange('sale_order_id')
    def _on_change_order(self): 
        
        """ It will change the Customer fields automatically as well as warehouse fields ,when selected sale order fields
        """
        self.customer = self.sale_order_id.partner_id.name
        self.warehouse = self.sale_order_id.warehouse_id.name
        
        
#         self.sale_order_id.invoice_ids.state
       
    
    @api.multi
    def GetProducts(self):
        """ It will set all the product ,when click the Get Product button
        """
        
        product_list=[]
        for line_id in self.sale_order_id.order_line:
            product_list.append(line_id.product_id.id)
        self.write({'products':[(6,0,product_list)]})   
        
    
#     @api.multi
#     def _calculate_invoice(self):
#         """ Calculates invoice"""
#           
#         self.invoice_paid_amount = self.sale_order_id.invoice_ids.amount_total_signed
#         self.invoice_remain_amount=self.sale_order_id.invoice_ids.residual_signed

          
            
            